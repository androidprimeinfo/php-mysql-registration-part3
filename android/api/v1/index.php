<?php

ini_set('display_errors', 1);
require_once '../include/DbHandler.php';
require '../libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

function verifyRequiredFields($required_fields) {
	
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
	
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
	
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoResponse(400, $response);
        $app->stop();
    }
	
}

function echoResponse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response);
}

// adding/registering user to database
 $app->post('/verifyEmail', function() use ($app) {
	
	// verifying required fields. will return error status if one is missing
    verifyRequiredFields(array('email'));

    $response = array();

    // reading post parameters
	$email = $app->request->post('email');

    $db = new DbHandler();
    $res = $db->isUserExists($email);

    if ($res == EMAIL_AVAILABLE) {
        $response["error"] = false;
        $response["message"] = "You can use this email";
    } else if ($res == EMAIL_ALREADY_TAKEN) {
        $response["error"] = true;
        $response["message"] = "This email is already taken";
    }
	
    // echo json response
    echoResponse(201, $response);	
	
});	

// adding/registering user to database
 $app->post('/register', function() use ($app) {
	
	// verifying required fields. will return error status if one is missing
    verifyRequiredFields(array('email', 'first_name', 'last_name', 'password'));

    $response = array();

     // reading post parameters
	$email = $app->request->post('email');
    $first_name = $app->request->post('first_name');
    $last_name = $app->request->post('last_name');
	$password = $app->request->post('password');

    $db = new DbHandler();
    $res = $db->createUser($email, $first_name, $last_name, $password);

    if ($res == USER_CREATED_SUCCESSFULLY) {
        $response["error"] = false;
        $response["message"] = "Successfully registered";
    } else if ($res == USER_CREATE_FAILED) {
        $response["error"] = true;
        $response["message"] = "An error occurred while registering";
    } else if ($res == USER_ALREADY_EXISTED) {
        $response["error"] = true;
        $response["message"] = "Already registered";
    }
	
    // echo json response
    echoResponse(201, $response);	
	
});	

// logging in
 $app->post('/login', function() use ($app) {
	
	// verifying required fields. will return error status if one is missing
    verifyRequiredFields(array('email', 'password'));

    $response = array();

     // reading post parameters
	$email = $app->request->post('email');
	$password = $app->request->post('password');

    $db = new DbHandler();
    $res = $db->verifyAccount($email, $password);

    if ($res) {
		$user = $db->userLogin($email);
        $response["error"] = false;
        $response["data"] = $user;
    } else {
        $response["error"] = true;
        $response["message"] = "Wrong username/password";
    } 
	
    // echo json response
    echoResponse(201, $response);	
	
});	

//updating user password
$app->post('/updatePassword', function() use ($app) {
	
	// check for required params
    verifyRequiredFields(array('id', 'password'));
	
	$db = new DbHandler();
	$response = array();
	
	$id = $app->request->post('id');
    $password = $app->request->post('password');
	
    $result = $db->updatePassword($password, $id);
			
	if ($result) {				
		$response["error"] = false;
		$response["message"] = "Password successfully updated";				
	} else {					
		$response["error"] = true;
		$response["message"] = "No changes made to password";				
	}
	
	// echo json response
	echoResponse(201, $response);
});	

// getting top 25 registered users
$app->get('/getRegisteredUsers', function() use ($app) {
	
	$db = new DbHandler();
    $users = $db->getRegisteredUsers();
	$response = array();
	
	if ($users != NULL) {		           				
		$response["error"] = false;
		$response['data'] = $users;			
    } else {
        $response['error'] = true;
        $response['message'] = "No registered users found";
    }

    echoResponse(200, $response);
});	
 		
$app->run();

?>