<?php

// Connecting to database
class DbConnect {

    private $conn;

    function __construct() {
    }

    function connect() {
		
        include_once dirname(__FILE__) . '/Config.php';

        $this->conn = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

        if (mysqli_connect_errno()) {
            echo "Connection to database failed: " . mysqli_connect_error();
        }
		
        return $this->conn;
    
	}

}

?>