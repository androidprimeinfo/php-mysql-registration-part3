<?php

//Database configuration
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');
define('DB_NAME', 'androidprime_registration');

//return status
define('USER_CREATED_SUCCESSFULLY', 0);
define('USER_CREATE_FAILED', 1);
define('USER_ALREADY_EXISTED', 2);

define('EMAIL_AVAILABLE', 0);
define('EMAIL_ALREADY_TAKEN', 1);

?>