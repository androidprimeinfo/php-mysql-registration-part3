<?php

class DbHandler {

    private $conn;

    function __construct() {
		
        require_once dirname(__FILE__) . '/DbConnect.php';
		require_once '../include/Config.php';
		require_once '../include/Constants.php';
        $db = new DbConnect();
        $this->conn = $db->connect();
		
    }
	
	public function isUserExists($email) {
        $stmt = $this->conn->prepare("SELECT id from users WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }
	
	public function verifyAccount($email, $password) {
        $stmt = $this->conn->prepare("SELECT id from users WHERE email = ? AND password = ?");
        $stmt->bind_param("ss", $email, $password);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }
	
	public function userLogin($email) {
				
        $stmt = $this->conn->prepare("SELECT 

				users.id,
				users.first_name,
				users.last_name 
				
				FROM users WHERE email = ?");
				
        $stmt->bind_param("s", $email);
		$stmt->execute();
		$stmt->bind_result($id, $first_name, $last_name);
		$stmt->fetch();
		
		$user = array();
		$user['id'] = $id;
		$user['first_name'] = $first_name;
		$user['last_name'] = $last_name;
		$user['email'] = $email;
		
		$stmt->close();
		return $user;
			
    }	
	
	public function createUser($email, $first_name, $last_name, $password) {
        
        $response = array();
       
        if (!$this->isUserExists($email)) {

			// change this query if you have other fields
            $stmt = $this->conn->prepare("INSERT INTO users(
				email, 
				first_name, 
				last_name, 
				password) 
				
				values(?, ?, ?, ?)");
				
            $stmt->bind_param("ssss",$email, $first_name, $last_name, $password);
            $result = $stmt->execute();
            $stmt->close();

			// return status of query
            if ($result) {
                return USER_CREATED_SUCCESSFULLY;
            } else {
                return USER_CREATE_FAILED;
            }
			
        } else {
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }
	
	public function updatePassword($password, $id){
			
		$stmt = $this->conn->prepare("UPDATE users 		
				set 
				users.password = ? 
					
				WHERE users.id = ?");
					
        $stmt->bind_param("ss", $password, $id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
		
	}
	
	public function getRegisteredUsers(){
		
		$stmt = $this->conn->prepare("SELECT 
		
				users.id, 
				users.email, 
				users.first_name, 
				users.last_name 
				
				FROM `users` 
				ORDER BY users.id DESC LIMIT 25");
		
		$users = array();
        if ($stmt->execute()) {
			
			$stmt->bind_result($id, $email, $first_name, $last_name);
        
			while($stmt->fetch()){
				array_push($users ,array(
				ID => $id,
				EMAIL => $email,
				FIRST_NAME => $first_name,
				LAST_NAME => $last_name
				));
			}

			$stmt->close();
			return $users;
		
		} else {
            return NULL;
        }
		
	}
	
}

?>