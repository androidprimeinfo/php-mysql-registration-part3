package info.androidprime.mysqlregistraion.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import info.androidprime.mysqlregistraion.R;
import info.androidprime.mysqlregistraion.base.MApplication;
import info.androidprime.mysqlregistraion.helper.CustomJsonRequest;
import info.androidprime.mysqlregistraion.model.LoginResponse;
import info.androidprime.mysqlregistraion.model.RegisterResponse;

public class Login extends AppCompatActivity {

    String URL_LOGIN = "http://192.168.0.26/android/api/v1/login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // giving reference to our text view
        TextView forgotPassword = (TextView) findViewById(R.id.text_view_forgot_password);
        TextView signUp = (TextView) findViewById(R.id.text_view_sign_up);
        final Button login = (Button) findViewById(R.id.button_log_in);

        // make an underlined text view
        forgotPassword.setText(Html.fromHtml("<u>Forgot Password?</u>"));
        signUp.setText(Html.fromHtml("<u>Register an Account</u>"));

        //open the register activity when clicked
        signUp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Login.this, Register.class);
                startActivity(i);
            }
        });

        login.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

    }

    private void login() {

        TextInputLayout tilUsername, tilPassword;
        final EditText editTextUsername, editTextPassword;

        tilUsername = (TextInputLayout) findViewById(R.id.text_input_layout_username);
        tilPassword = (TextInputLayout) findViewById(R.id.text_input_layout_password);
        editTextUsername = (EditText) findViewById(R.id.edit_text_username);
        editTextPassword = (EditText) findViewById(R.id.edit_text_password);

        if (editTextUsername.getText().toString().trim().isEmpty()) {
            tilUsername.setError("Enter username");
            return;
        } else {
            tilUsername.setErrorEnabled(false);
        }

        if (editTextPassword.getText().toString().trim().isEmpty()) {
            tilPassword.setError("Enter password");
            return;
        } else {
            tilPassword.setErrorEnabled(false);
        }

        //log in username and password
        CustomJsonRequest jsonReq = new CustomJsonRequest(Request.Method.POST, URL_LOGIN,
                null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //show a toast then go back to login activity if successful
                Gson gson = new GsonBuilder().create();
                LoginResponse loginResponse = gson.fromJson(response.toString(), LoginResponse.class);
                
                if (loginResponse.isSuccess()) {
                    Toast.makeText(Login.this, "Welcome " + loginResponse.getUserAccount().getFirstName(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Login.this, Home.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(Login.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Login.this, "There is a problem connecting to the database.", Toast.LENGTH_SHORT).show();
            }

        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", editTextUsername.getText().toString().trim());
                params.put("password", editTextPassword.getText().toString().trim());
                return params;
            }

        };

        MApplication.getInstance().addToRequestQueue(jsonReq);

    }

}
