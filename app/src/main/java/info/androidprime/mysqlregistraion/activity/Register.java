package info.androidprime.mysqlregistraion.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.androidprime.mysqlregistraion.R;
import info.androidprime.mysqlregistraion.base.MApplication;
import info.androidprime.mysqlregistraion.fragment.RegisterSlide1;
import info.androidprime.mysqlregistraion.fragment.RegisterSlide2;
import info.androidprime.mysqlregistraion.fragment.RegisterSlide3;
import info.androidprime.mysqlregistraion.helper.CustomJsonRequest;
import info.androidprime.mysqlregistraion.model.RegisterResponse;

public class Register extends AppCompatActivity {

    static final String TAG = Register.class.getSimpleName();

    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;

    private LinearLayout pageControlLayout;
    private TextView[] pageDots;
    private Button btnBack, btnNext;
    private RegisterSlide1 registerSlide1;
    private RegisterSlide2 registerSlide2;
    private RegisterSlide3 registerSlide3;

    String URL_REGISTER = "http://192.168.0.26/android/api/v1/register";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        pageControlLayout = (LinearLayout) findViewById(R.id.layoutDots);
        btnBack = (Button) findViewById(R.id.btn_back);
        btnNext = (Button) findViewById(R.id.btn_next);

        // create a pager adapter then add the fragments
        pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(RegisterSlide1.createInstance(), "Slide1");
        pagerAdapter.addFragment(RegisterSlide2.createInstance(), "Slide2");
        pagerAdapter.addFragment(RegisterSlide3.createInstance(), "Slide3");
        viewPager.setAdapter(pagerAdapter);

        // add our page control in bottom
        updatePageControl(0);

        // getting the fragments in our pager adapter so we can get the views inside
        registerSlide1 = (RegisterSlide1) pagerAdapter.getItem(0);
        registerSlide2 = (RegisterSlide2) pagerAdapter.getItem(1);
        registerSlide3 = (RegisterSlide3) pagerAdapter.getItem(2);

        // add a page change listener so we can change the button text
        viewPager.addOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                // update the page control
                updatePageControl(position);

                if (position == pagerAdapter.getCount() - 1) {
                    // if in last page. make button's text to Register
                    btnNext.setText(getString(R.string.register));
                } else {
                    btnNext.setText(getString(R.string.next));
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int current = viewPager.getCurrentItem() - 1;
                if (current >= 0) {
                    // move to our previous registration screen
                    viewPager.setCurrentItem(current);
                } else {
                    // go back to our login activity
                    finish();
                }

            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = viewPager.getCurrentItem() + 1;
                checkInput(current);
            }
        });

    }

    private void checkInput(int current) {

        if (current < pagerAdapter.getCount()) {

            //checks input
            switch (viewPager.getCurrentItem()) {

                case 0:
                    if (!registerSlide1.hasFirstName() || !registerSlide1.hasLastName()) {
                        return;
                    }
                    break;

                case 1:
                    if (!registerSlide2.hasEmail()) {
                        return;
                    }
                    break;

            }

            // move to next screen
            viewPager.setCurrentItem(current);

        } else {

            // check password inputs
            if (!registerSlide3.hasPassword1() || !registerSlide3.hasPassword2()) {
                return;
            }

            // check if both password is matched
            if (!registerSlide3.isPasswordMatch()) {
                return;
            }

            //saving info to our database
            CustomJsonRequest jsonReq = new CustomJsonRequest(Request.Method.POST, URL_REGISTER,
                    null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    //show a toast then go back to login activity if successful
                    Gson gson = new GsonBuilder().create();
                    RegisterResponse registerResponse = gson.fromJson(response.toString(), RegisterResponse.class);

                    if (registerResponse.isSuccess()) {
                        Toast.makeText(Register.this, "Successfully registered", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(Register.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(Register.this, "There is a problem connecting to the database.", Toast.LENGTH_SHORT).show();
                }

            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("first_name", registerSlide1.getFirstName());
                    params.put("last_name", registerSlide1.getLastName());
                    params.put("email", registerSlide2.getEmail());
                    params.put("password", registerSlide3.getPassword());
                    return params;
                }

            };

            MApplication.getInstance().addToRequestQueue(jsonReq);

        }

    }

    private void updatePageControl(int currentPage) {

        pageDots = new TextView[pagerAdapter.getCount()];

        // this will get the color of active and inactive dots.
        // you can change the color of each dot
        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        //remove all dots before adding new dots
        pageControlLayout.removeAllViews();

        for (int i = 0; i < pageDots.length; i++) {
            pageDots[i] = new TextView(this);
            pageDots[i].setText(Html.fromHtml("&#8226;")); // this is a bullet in html code
            pageDots[i].setTextSize(35);
            pageDots[i].setTextColor(colorsInactive[currentPage]);
            pageControlLayout.addView(pageDots[i]);
        }

        // change the color of dots base on current screen
        if (pageDots.length > 0)
            pageDots[currentPage].setTextColor(colorsActive[currentPage]);

    }

    static class PagerAdapter extends FragmentPagerAdapter {

        // this class will hold all of our fragments.
        // you can use this to retrieve views inside the fragment
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }

        public int getCurrentItem() {
            return getCurrentItem();
        }

    }

}
