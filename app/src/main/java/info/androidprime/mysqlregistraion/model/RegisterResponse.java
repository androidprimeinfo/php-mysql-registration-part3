package info.androidprime.mysqlregistraion.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mark Cua on 2/25/2018.
 */

public class RegisterResponse {

    @SerializedName("error")
    private Boolean hasError;

    @SerializedName("message")
    private String message;

    public RegisterResponse(Boolean hasError, String message) {
        this.hasError = hasError;
        this.message = message;
    }

    public Boolean isSuccess() {
        return !hasError;
    }

    public String getMessage() {
        return message;
    }

}
