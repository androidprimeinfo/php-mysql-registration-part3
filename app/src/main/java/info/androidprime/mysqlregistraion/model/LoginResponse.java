package info.androidprime.mysqlregistraion.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mark Cua on 2/25/2018.
 */

public class LoginResponse {

    @SerializedName("error")
    private Boolean hasError;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private UserAccount userAccount;

    public LoginResponse(Boolean hasError, String message, UserAccount userAccount) {
        this.hasError = hasError;
        this.message = message;
        this.userAccount = userAccount;
    }

    public Boolean isSuccess() {
        return !hasError;
    }

    public String getMessage() {
        return message;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }
}
