package info.androidprime.mysqlregistraion.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import info.androidprime.mysqlregistraion.R;

/**
 * Created by Mark Cua on 1/22/2017.
 */

public class RegisterSlide3 extends Fragment {

    private TextInputLayout textInputLayoutPassword1, textInputLayoutPassword2;
    private EditText password1, password2;

    public static RegisterSlide3 createInstance() {
        RegisterSlide3 registerSlide3 = new RegisterSlide3();
        return registerSlide3;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.register_slide3, container, false);
        textInputLayoutPassword1 = (TextInputLayout) rootView.findViewById(R.id.text_input_layout_password);
        textInputLayoutPassword2 = (TextInputLayout) rootView.findViewById(R.id.text_input_layout_password2);
        password1 = (EditText) rootView.findViewById(R.id.edit_text_password);
        password2 = (EditText) rootView.findViewById(R.id.edit_text_password2);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public boolean hasPassword1() {

        if (password1.getText().toString().trim().isEmpty()) {
            textInputLayoutPassword1.setError("Enter password");
            return false;
        } else {
            textInputLayoutPassword1.setErrorEnabled(false);
            return true;
        }

    }

    public String getPassword() {
        return password1.getText().toString().trim();
    }

    public boolean hasPassword2() {

        if (password2.getText().toString().trim().isEmpty()) {
            textInputLayoutPassword2.setError("Confirm password");
            return false;
        } else {
            textInputLayoutPassword2.setErrorEnabled(false);
            return true;
        }

    }

    public boolean isPasswordMatch() {

        if (!password2.getText().toString().trim().equals(password1.getText().toString().trim())) {
            textInputLayoutPassword2.setError("Passwords not match");
            return false;
        } else {
            textInputLayoutPassword2.setErrorEnabled(false);
            return true;
        }

    }

}
