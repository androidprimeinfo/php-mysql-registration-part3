package info.androidprime.mysqlregistraion.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import info.androidprime.mysqlregistraion.R;

/**
 * Created by Mark Cua on 1/22/2017.
 */

public class RegisterSlide2 extends Fragment {

    private TextInputLayout textInputLayout;
    private EditText email;

    public static RegisterSlide2 createInstance() {
        RegisterSlide2 registerSlide2 = new RegisterSlide2();
        return registerSlide2;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.register_slide2, container, false);
        textInputLayout = (TextInputLayout) rootView.findViewById(R.id.text_input_layout_email);
        email = (EditText) rootView.findViewById(R.id.edit_text_email);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public boolean hasEmail() {
        if (email.getText().toString().trim().isEmpty()) {
            textInputLayout.setError("Enter your email address");
            return false;
        } else {

            if (isValidEmail(email.getText().toString().trim())) {
                textInputLayout.setErrorEnabled(false);
                return true;
            } else {
                textInputLayout.setError("Enter a valid email address");
                return false;
            }
        }
    }

    public String getEmail() {
        return email.getText().toString().trim();
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

}
