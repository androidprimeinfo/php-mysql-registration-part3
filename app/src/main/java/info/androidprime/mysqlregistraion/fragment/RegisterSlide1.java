package info.androidprime.mysqlregistraion.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import info.androidprime.mysqlregistraion.R;

/**
 * Created by Mark Cua on 1/22/2017.
 */

public class RegisterSlide1 extends Fragment {

    private TextInputLayout textInputLayoutFirstName, textInputLayoutLastName;
    private EditText firstName, lastName;

    public static RegisterSlide1 createInstance() {
        RegisterSlide1 registerSlide1 = new RegisterSlide1();
        return registerSlide1;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.register_slide1, container, false);
        textInputLayoutFirstName = (TextInputLayout) rootView.findViewById(R.id.text_input_layout_firstname);
        textInputLayoutLastName = (TextInputLayout) rootView.findViewById(R.id.text_input_layout_lastname);
        firstName = (EditText) rootView.findViewById(R.id.edit_text_firstname);
        lastName = (EditText) rootView.findViewById(R.id.edit_text_lastname);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public boolean hasFirstName() {

        if (firstName.getText().toString().trim().isEmpty()) {
            textInputLayoutFirstName.setError("Enter your first name");
            return false;
        } else {
            textInputLayoutFirstName.setErrorEnabled(false);
            return true;
        }

    }

    public boolean hasLastName() {

        if (lastName.getText().toString().trim().isEmpty()) {
            textInputLayoutLastName.setError("Enter your last name");
            return false;
        } else {
            textInputLayoutLastName.setErrorEnabled(false);
            return true;
        }

    }

    public String getLastName(){
        return lastName.getText().toString().trim();
    }

    public String getFirstName(){
        return firstName.getText().toString().trim();
    }

    public String getName() {
        return firstName.getText().toString().trim() + " " + lastName.getText().toString().trim();
    }

}
